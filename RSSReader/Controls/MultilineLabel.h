//
//  AutoExpandedLabel.h
//  PrivateGuide
//
//  Created by Alexandr Chernyshev on 12/06/15.
//  Copyright (c) 2015 Alexandr Chernyshev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MultilineLabel : UILabel

@end
