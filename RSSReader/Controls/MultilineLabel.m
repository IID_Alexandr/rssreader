//
//  AutoExpandedLabel.m
//  PrivateGuide
//
//  Created by Alexandr Chernyshev on 12/06/15.
//  Copyright (c) 2015 Alexandr Chernyshev. All rights reserved.
//

#import "MultilineLabel.h"

#import "UIView+IDView.h"

@implementation MultilineLabel

-(void)prepareAppearence
{
    [super prepareAppearence];
    self.preferredMaxLayoutWidth = self.bounds.size.width;
}

- (void)setBounds:(CGRect)bounds {
    [super setBounds:bounds];
    if ((self.numberOfLines == 0 || self.numberOfLines > 1)) {// && bounds.size.width != self.preferredMaxLayoutWidth) {
        self.preferredMaxLayoutWidth = bounds.size.width;//236.0f;//self.bounds.size.width;
        [self updateConstraintsIfNeeded];
        [self invalidateIntrinsicContentSize];
    }
}

@end
