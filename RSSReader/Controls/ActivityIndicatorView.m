//
//  ActivityIndicatorView.m
//  CODE
//
//  Created by Alexandr Chernyshev on 8/6/14.
//  Copyright (c) 2014 ImproveIT. All rights reserved.
//

#import "ActivityIndicatorView.h"

static CGFloat const kActivityBackgroundScale = 2.5;

@interface ActivityIndicatorView ()
@property (nonatomic, weak) UIActivityIndicatorView *   activityIndicatorView;
@property (nonatomic, weak) UIImageView *               backgroundImageView;
@property (nonatomic, weak) UIView *                    blackOverlayView;

@end

@implementation ActivityIndicatorView

#pragma mark - initialization
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupView];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self) {
        [self setupView];
    }
    return self;
}

- (void)setupView
{
    self.backgroundColor = [UIColor clearColor];
    self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.userInteractionEnabled = NO;
    
    UIImageView *backgroundView = [[UIImageView alloc]initWithFrame:self.bounds];
    backgroundView.contentMode = UIViewContentModeScaleToFill;
    [self addSubview:backgroundView];
    self.backgroundImageView = backgroundView;

    UIActivityIndicatorView *actIndicatorView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    CGFloat a = actIndicatorView.bounds.size.width*kActivityBackgroundScale;
    UIView * blackView = [[UIView alloc]initWithFrame:CGRectMake(CGRectGetMidX(self.bounds) - a/2, CGRectGetMidY(self.bounds) - a/2, a, a)];
    blackView.clipsToBounds = YES;
    blackView.backgroundColor = [UIColor blackColor];
    blackView.layer.cornerRadius = a/5;
    [self addSubview:blackView];
    self.blackOverlayView = blackView;
    
    actIndicatorView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    [self addSubview:actIndicatorView];
    self.activityIndicatorView = actIndicatorView;
    
    [self setNeedsLayout];
    [self layoutIfNeeded];
}

#pragma mark - public methods
- (void)startActivity:(UIView *)view
{
    self.hidden = NO;
    self.activityIndicatorView.hidden = NO;
    self.blackOverlayView.hidden = NO;
    [self.activityIndicatorView startAnimating];
}

- (void)finishActivity
{
    self.hidden = YES;
    self.activityIndicatorView.hidden = YES;
    self.blackOverlayView.hidden = YES;
    self.backgroundImageView.image = nil;
    [self.activityIndicatorView stopAnimating];
}

#pragma mark - UIView methods
- (void)willMoveToSuperview:(UIView *)newSuperview
{
    [super willMoveToSuperview:newSuperview];
    self.frame = newSuperview.bounds;
    [self setNeedsLayout];
    [self layoutIfNeeded];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.blackOverlayView.center = CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds));
    self.backgroundImageView.frame = self.bounds;
    self.activityIndicatorView.center = CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds));
}
@end
