//
//  Constants.h
//  RSSReader
//
//  Created by Alexandr Chernyshev on 27/11/15.
//  Copyright © 2015 Improve Digital. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

static NSInteger const  kNoInternetCode                 = 100002;
static NSString * const kAddNewChannelIdentifier        = @"com.rssreader.addnewchannel";
static NSInteger const  kMaxNumberOfShortcurChannels    = 3;

static NSString * const kChannelUserActivityType        = @"com.rssreader.channels";
static NSString * const kFeedSearchDomainType           = @"com.rssreader.feeds";

#endif
