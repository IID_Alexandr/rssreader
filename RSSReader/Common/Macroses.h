//
//  Macroses.h
//  RSSReader
//
//  Created by Alexandr Chernyshev on 27/11/15.
//  Copyright © 2015 Improve Digital. All rights reserved.
//

#ifndef Macroses_h
#define Macroses_h

#define LOC(key)                                        NSLocalizedString((key), @"")
#define NIL_TO_NULL(obj)                                (obj != nil) ? obj : [NSNull null]
#define NULL_TO_NIL(obj)                                ({ __typeof__ (obj) __obj = (obj); __obj == [NSNull null] ? nil : obj; })

#endif
