//
//  APIBodyItem.m
//  RSSReader
//
//  Created by Alexandr Chernyshev on 25/07/15.
//  Copyright (c) 2015 Alexandr Chernyshev. All rights reserved.
//

#import "APIBodyItem.h"

@implementation APIBodyItem

- (instancetype)initWithJPEGData:(NSData *)data name:(NSString *)name
{
    return [self initWithData:data name:name fileName:[name stringByAppendingPathExtension:@"jpg"] mimeType:@"image/jpeg"];
}

- (instancetype)initWithData:(NSData *)data name:(NSString *)name fileName:(NSString *)fileName mimeType:(NSString *)mimeType
{
    self = [super init];
    if(self) {
        _name = name;
        _data = data;
        _fileName = fileName;
        _mimeType = mimeType;
    }
    return self;
}
@end
