//
//  Feed+Cache.m
//  RSSReader
//
//  Created by Alexandr Chernyshev on 27/11/15.
//  Copyright © 2015 Improve Digital. All rights reserved.
//

#import "Feed+Cache.h"

#import "FeedCache.h"

@implementation Feed (Cache)

- (instancetype)initWithCache:(FeedCache *)cache
{
    self = [super initWithCache:cache];
    if(self) {
        self.title = cache.title;
        self.details = cache.details;
        self.date = cache.date;
        self.url = [NSURL URLWithString:cache.url];
        self.imageURL = [NSURL URLWithString:cache.imageURL];
        self.channelIdentifier = cache.channelIdentifier;
    }
    return self;
}

- (FeedCache *)cachedObjectWithClient:(CacheDataManager *)cacheClient
{
    FeedCache *feed = (FeedCache *)[super cachedObjectWithClient:cacheClient];
    feed.title = self.title;
    feed.details = self.details;
    feed.date = self.date;
    feed.url = self.url.absoluteString;
    feed.imageURL = self.imageURL.absoluteString;
    feed.channelIdentifier = self.channelIdentifier;
    return feed;
}
@end
