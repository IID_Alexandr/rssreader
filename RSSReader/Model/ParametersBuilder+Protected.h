//
//  ParametersBuilder_Protected.h
//  RSSReader
//
//  Created by iOS developer on 03/08/15.
//  Copyright (c) 2015 ImproveIT. All rights reserved.
//

#import "ParametersBuilder.h"

@interface ParametersBuilder ()

+ (instancetype)builder;
@end
