//
//  ChannelCache.h
//  RSSReader
//
//  Created by Alexandr Chernyshev on 27/11/15.
//  Copyright © 2015 Improve Digital. All rights reserved.
//

#import "BaseEntityCache.h"

@interface ChannelCache : BaseEntityCache
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *url;

@end
