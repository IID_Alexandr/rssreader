//
//  CacheProtocol.h
//  RSSReader
//
//  Created by iOS developer on 18/08/15.
//  Copyright (c) 2015 ImproveIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CacheDataManager.h"

@protocol CacheProtocol <NSObject>

-(void)cache:(CacheDataManager *)cacheClient;

@end
