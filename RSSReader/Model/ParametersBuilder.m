//
//  ParametersBuilder.m
//  RSSReader
//
//  Created by Alexandr Chernyshev on 10/06/15.
//  Copyright (c) 2015 Alexandr Chernyshev. All rights reserved.
//

#import "ParametersBuilder.h"

#import "ParametersBuilder+Protected.h"

@interface ParametersBuilder()
@property (nonatomic, strong) NSMutableDictionary *internalDict;

@end

@implementation ParametersBuilder

#pragma mark - Subclassing 
- (instancetype)init
{
    self = [super init];
    if(self) {
        _internalDict = [NSMutableDictionary dictionary];
    }
    return self;
}

#pragma mark - class methods
+ (instancetype)builder
{
    return [[[self class] alloc] init];
}

+ (instancetype)withObject:(id)object forKey:(NSString *)key
{
    return [[self builder] withObject:object forKey:key];
}

+ (instancetype)withParameters:(ParametersBuilder *)parameters
{
    return [[self builder] withParameters:parameters];
}

#pragma mark - public methods
- (instancetype)withObject:(id)object forKey:(NSString *)key
{
    if(object) {
        [self.internalDict setObject:object forKey:key];
    }
    return self;
}

- (instancetype)withParameters:(ParametersBuilder *)parameters
{
    [parameters.internalDict enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        [self withObject:obj forKey:key];
    }];
    return self;
}

#pragma mark - Serialization
- (NSDictionary *)params
{
    return self.internalDict;
}

- (NSData *)jsonParams
{
    NSError *jsonSerializationError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self.internalDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&jsonSerializationError];
    
#if DEBUG
    if(!jsonSerializationError) {
        NSString *serJSON = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"Serialized JSON: %@", serJSON);
    }
    else {
        NSLog(@"JSON Encoding Failed: %@", [jsonSerializationError localizedDescription]);
    }
#endif
    return jsonData;
}

- (NSData *)plainDataParams
{
    return [self.queryString dataUsingEncoding:NSUTF8StringEncoding];
}

- (NSString *)jsonString
{
    NSError *error = nil;
    NSString *jsonString = [[NSString alloc] initWithData:
                            [NSJSONSerialization dataWithJSONObject:self
                                                            options:NSJSONWritingPrettyPrinted
                                                              error:&error]
                                                 encoding:NSUTF8StringEncoding];
    
    if (error) {
        NSLog(@"Не удалось сериализовать информацию: %@", error.localizedDescription);
    }
    return jsonString;
}

- (NSString *)queryString
{
    NSMutableArray *pairs = [NSMutableArray array];
    [self.internalDict enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        NSString *pairString = [NSString stringWithFormat:@"%@=%@", key, obj];
        [pairs addObject:pairString];
    }];
    return [pairs componentsJoinedByString:@"&"];
}
@end
