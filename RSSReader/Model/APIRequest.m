//
//  SFAPIRequest.m
//  RSSReader
//
//  Created by Alexandr Chernyshev on 24/06/15.
//  Copyright (c) 2015 ImproveIT. All rights reserved.
//

#import "APIRequest.h"

#import <AFNetworking/AFNetworking.h>

#import "NSString+Escaping.h"

@interface APIRequest()
@property (nonatomic, assign) RequestMethod method;
@property (nonatomic, copy) NSString *urlPath;
@property (nonatomic, readonly) NSMutableDictionary *parametersStorage;
@property (nonatomic, strong) NSMutableSet *apiBodyItems;

@end

@implementation APIRequest

#pragma mark - initialization
- (instancetype)initWithMethod:(RequestMethod)method
                       urlPath:(NSString *)urlPath
{
    self = [super init];
    if(self) {
        _apiBodyItems = [NSMutableSet set];
        _parametersStorage = [NSMutableDictionary dictionary];
        _method = method;
        _urlPath = [urlPath copy];
    }
    return self;
}

#pragma mark - properties
- (NSURLRequest *)urlRequest
{
    AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
    NSMutableURLRequest *request = nil;
    switch (self.method) {
        case RequestMethodGet: {
            request = [requestSerializer requestWithMethod:@"GET" URLString:self.urlPath parameters:nil error:nil];
        }
            break;
        case RequestMethodPost: {
            request = [requestSerializer multipartFormRequestWithMethod:@"POST"
                                                    URLString:self.urlPath
                                                   parameters:nil
                                    constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                                        for(APIBodyItem *item in self.apiBodyItems) {
                                            [formData appendPartWithFileData:item.data name:item.name fileName:item.fileName mimeType:item.mimeType];
                                        }
                                    }
                                                        error:nil];
        }
            break;
        default: {
            NSAssert(NO, @"Unknown http method");
        }
            break;
    }
    [self.parametersStorage enumerateKeysAndObjectsUsingBlock:^(NSNumber *key, ParametersBuilder *obj, BOOL *stop) {
        [self configureRequest:request withParameters:obj serializationType:key.integerValue];
    }];
    return request;
}

#pragma mark - public methods
- (void)addParametersBuilder:(ParametersBuilder *)parametersBuilder forSerializationType:(ParametersSerializationType)serializationType
{
    ParametersBuilder *builder = [self.parametersStorage objectForKey:@(serializationType)];
    if(!builder) {
        builder = parametersBuilder;
    }
    else {
        [builder withParameters:parametersBuilder];
    }
    
    if(builder) {
        [self.parametersStorage setObject:builder forKey:@(serializationType)];
    }
}

- (void)addBodyItem:(APIBodyItem *)bodyItem
{
    if(bodyItem)
    {
        [self.apiBodyItems addObject:bodyItem];
    }
}

#pragma mark - working methods
- (void)configureRequest:(NSMutableURLRequest *)request
          withParameters:(ParametersBuilder *)parametersBuilder
       serializationType:(ParametersSerializationType)serializationType
{
    switch (serializationType) {
        case ParametersSerializationTypeHeader: {
            NSDictionary *params = parametersBuilder.params;
            [params enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                [request setValue:obj forHTTPHeaderField:key];
            }];
        }
            break;
        case ParametersSerializationTypeURL: {
            NSURL *url = request.URL;
            request.URL = [NSURL URLWithString:[[url absoluteString] stringByAppendingFormat:url.query ? @"&%@" : @"?%@", [parametersBuilder.queryString oloUrlEncodedString]]];
        }
            break;
        case ParametersSerializationTypeBody: {
            NSMutableData *data = [NSMutableData dataWithData:request.HTTPBody];
            [data appendData:[parametersBuilder plainDataParams]];
            request.HTTPBody = data;
        }
            break;
        case ParametersSerializationTypeJSON: {
            NSData *jsonData = [parametersBuilder jsonParams];
            [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
            [request setValue:@(jsonData.length).stringValue forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
            
            NSMutableData *data = [NSMutableData dataWithData:request.HTTPBody];
            [data appendData:jsonData];
            request.HTTPBody = data;
        }
            break;
            
        default: {
            NSAssert(NO, @"Unknown Serialization Type");
        }
            break;
    }
}

@end
