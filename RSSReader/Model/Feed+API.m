//
//  Feed+API.m
//  RSSReader
//
//  Created by Alexandr Chernyshev on 27/11/15.
//  Copyright © 2015 Improve Digital. All rights reserved.
//

#import "Feed+API.h"

#import <DTCoreText/DTCoreText.h>

#import "NSDictionary+SafeParsing.h"

static NSString * const kFeedIDKey    = @"guid";
static NSString * const kDetailsKey   = @"description";
static NSString * const kFeedURLKey   = @"link";
static NSString * const kDateKey      = @"pubDate";
static NSString * const kTitleKey     = @"title";
static NSString * const kImageKey     = @"src=";

@implementation Feed (API)

#pragma mark - parsing methods
+ (NSString *)idKey
{
    return kFeedIDKey;
}

- (void)updateWithDictionary:(NSDictionary *)values
{
    self.title = [self stringFromHTMLString:[values stringAtKey:kTitleKey]];
    self.url = [values urlAtKey:kFeedURLKey];
    
    NSString *dateString = [values stringAtKey:kDateKey];
    self.date = [self dateFromString:dateString];
    
    NSString *detailsHTMLText = [values stringAtKey:kDetailsKey];
    self.details = [self stringFromHTMLString:detailsHTMLText];
    self.imageURL = [self imageLinkFromDescription:detailsHTMLText];
}

#pragma mark - working methods
- (NSURL *)imageLinkFromDescription:(NSString *)description
{
    NSRange range = [description rangeOfString:kImageKey];
    if(range.location != NSNotFound) {
        NSString *substring = [description substringFromIndex:range.location + range.length];
        NSURL *link = [self linkFromString:substring borders:@"&quot;"];
        if(!link) {
            link = [self linkFromString:substring borders:@"\""];
        }
        return link;
    }
    return nil;
}

- (NSDate *)dateFromString:(NSString *)dateString
{
    static NSDateFormatter *dateFormatter = nil;
    if(!dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"EEE, dd MMM yy HH:mm:ss ZZZ"];
    }
    return [dateFormatter dateFromString:dateString];
}

- (NSString *)stringFromHTMLString:(NSString *)text
{
    NSData *data = [text dataUsingEncoding:NSUTF8StringEncoding];
    
    NSAttributedString *attrString = [[NSAttributedString alloc] initWithHTMLData:data
                                                                          options:@{DTUseiOS6Attributes: [NSNumber numberWithBool:YES]}
                                                               documentAttributes:NULL];
    NSString *result = [[attrString string] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    return result;
}


- (NSURL *)linkFromString:(NSString *)string borders:(NSString *)border
{
    NSRange startRange = [string rangeOfString:border];
    if(startRange.location != NSNotFound) {
        NSString *midleString = [string substringFromIndex:startRange.location + startRange.length];
        
        NSRange endRange = [midleString rangeOfString:border];
        NSString *link = [midleString substringToIndex:endRange.location];
        return [NSURL URLWithString:link];
    }
    else {
        return nil;
    }
}


@end
