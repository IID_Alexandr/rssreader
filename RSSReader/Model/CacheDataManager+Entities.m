//
//  CacheDataManager+Entities.m
//  RSSReader
//
//  Created by Alexandr Chernyshev on 12/06/15.
//  Copyright (c) 2015 Alexandr Chernyshev. All rights reserved.
//

#import "CacheDataManager+Entities.h"

#import "BaseEntity.h"
#import "IDUtils.h"

@implementation CacheDataManager (Entities)

- (NSArray *)entitiesWithClass:(Class)aClass
{
    NSArray *cachedEntities = [self objectsWithType:[aClass cachedObjectClass]];
    return [self entitiesFromCache:cachedEntities withClass:aClass];
}

- (BaseEntity *)entityWithClass:(Class)aClass identifier:(NSString *)identifier
{
    return [[self entitiesWithClass:aClass identifiers:@[identifier]] firstObject];
}

- (NSArray *)entitiesWithClass:(Class)aClass identifiers:(NSArray *)identifiers
{
    if(identifiers) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"identifier IN %@", identifiers];
        return [self entitiesWithClass:aClass predicate:predicate];
    }
    else {
        return nil;
    }
}

- (BaseEntity *)entityWithClass:(Class)aClass predicate:(NSPredicate *)predicate
{
    return [[self entitiesWithClass:aClass predicate:predicate] firstObject];
}

- (id)performDatabaseRequestObject:(CacheRequest *)cacheRequest
{
    Class aClass = NSClassFromString(cacheRequest.entityName);
    NSArray *entities = [self entitiesWithClass:aClass predicate:cacheRequest.predicate];
    if (cacheRequest.many) {
        if(entities.count) {
            return entities;
        }
        else {
            return nil;
        }
    }
    else {
        return [entities firstObject];
    }
}

- (NSArray *)entitiesWithClass:(Class)aClass predicate:(NSPredicate *)predicate
{
    NSFetchRequest *cacheRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([aClass cachedObjectClass])];
    cacheRequest.predicate = predicate;
    NSArray *cachedEntities = [self getEntitiesWithRequest:cacheRequest];
    return [self entitiesFromCache:cachedEntities withClass:aClass];
}

#pragma mark - private methods
- (NSArray *)entitiesFromCache:(NSArray *)cache withClass:(Class)aClass
{
    return [cache mapLike:^BaseEntity *(BaseEntityCache *object) {
        return [[[aClass class]alloc]initWithCache:object];
    }];
}
@end
