//
//  SFAPIDataManager.m
//  RSSReader
//
//  Created by Alexandr Chernyshev on 24/06/15.
//  Copyright (c) 2015 ImproveIT. All rights reserved.
//

#import "XMLDataManager.h"

#import <ReactiveCocoa/ReactiveCocoa.h>

#import "XMLParser.h"

@implementation XMLDataManager

- (RACSignal *)performRequest:(APIRequest *)request
{
    return [[super performRequest:request] flattenMap:^RACStream *(id value) {
        NSString *xmlString = [[NSString alloc]initWithData:value encoding:NSUTF8StringEncoding];
        XMLParser *parser = [XMLParser parserWithXMLString:xmlString];
        NSError *error = nil;
        NSArray *result = [parser parsedItems:&error];
        if(result) {
            return [RACSignal return:result];
        }
        else {
            return [RACSignal error:error];
        }
    }];
}

@end
