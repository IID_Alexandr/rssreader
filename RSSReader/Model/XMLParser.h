//
//  RecipesParser.h
//  Chefdaw
//
//  Created by Alexandr Chernyshev on 10/26/13.
//  Copyright (c) 2013 Alexandr Chernyshev. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^XMLParserResultHandler)(NSArray *recipes, NSError *error);

@interface XMLParser : NSObject

+ (id)parserWithXMLString:(NSString *)xmlString;
- (id)initWithXMLString:(NSString *)xmlString;

- (NSArray *)parsedItems:(NSError *__autoreleasing*)error;
@end
