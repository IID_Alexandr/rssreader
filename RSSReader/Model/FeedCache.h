//
//  FeedCache.h
//  RSSReader
//
//  Created by Alexandr Chernyshev on 27/11/15.
//  Copyright © 2015 Improve Digital. All rights reserved.
//

#import "BaseEntityCache.h"

@interface FeedCache : BaseEntityCache
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *details;
@property (nonatomic, retain) NSDate *date;
@property (nonatomic, retain) NSString *url;
@property (nonatomic, retain) NSString *imageURL;
@property (nonatomic, retain) NSString *channelIdentifier;

@end
