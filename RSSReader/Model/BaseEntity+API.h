//
//  BaseEntity+Parser.h
//  RSSReader
//
//  Created by Alexandr Chernyshev on 23/05/15.
//  Copyright (c) 2015 Alexandr Chernyshev. All rights reserved.
//

#import "BaseEntity.h"

@interface BaseEntity (API)

+ (NSString *)idKey;
+ (NSString *)defaultIdentifierWithDictionary:(NSDictionary *)values;
+ (instancetype)objectWithDictionary:(NSDictionary *)values;
+ (NSArray *)objectsArrayWithResponse:(NSArray *)response;
- (void)updateWithDictionary:(NSDictionary *)values;
@end
