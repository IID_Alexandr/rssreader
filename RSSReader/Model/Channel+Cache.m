//
//  Channel+Cache.m
//  RSSReader
//
//  Created by Alexandr Chernyshev on 28/11/15.
//  Copyright © 2015 Improve Digital. All rights reserved.
//

#import "Channel+Cache.h"

#import "ChannelCache.h"

@implementation Channel (Cache)

- (instancetype)initWithCache:(ChannelCache *)cache
{
    self = [super initWithCache:cache];
    if(self) {
        self.title = cache.title;
        self.urlString = cache.url;
    }
    return self;
}

- (ChannelCache *)cachedObjectWithClient:(CacheDataManager *)cacheClient
{
    ChannelCache *channel = (ChannelCache *)[super cachedObjectWithClient:cacheClient];
    channel.title = self.title;
    channel.url = self.urlString;
    return channel;
}
@end
