//
//  HeaderParametersBuilder.h
//  RSSReader
//
//  Created by Alexandr Chernyshev on 29/06/15.
//  Copyright (c) 2015 Improve Digital. All rights reserved.
//

#import "ParametersBuilder.h"

@interface HeaderParametersBuilder : ParametersBuilder

+ (instancetype)withContentType:(NSString *)contentType;
+ (instancetype)withAccept:(NSString *)accept;
+ (instancetype)withContentLength:(NSInteger)length;

- (instancetype)withContentType:(NSString *)contentType;
- (instancetype)withAccept:(NSString *)accept;
- (instancetype)withContentLength:(NSInteger)length;
@end
