//
//  BaseEntity+CacheParsing.m
//  RSSReader
//
//  Created by Alexandr Chernyshev on 27/05/15.
//  Copyright (c) 2015 Alexandr Chernyshev. All rights reserved.
//

#import "BaseEntity+Cache.h"

#import "CacheDataManager.h" 

@implementation BaseEntity (Cache)

+ (Class)cachedObjectClass
{
    NSString *className = NSStringFromClass([self class]);
    NSString *cacheClassName = [className stringByAppendingString:@"Cache"];
    return NSClassFromString(cacheClassName);
}

- (instancetype)initWithCache:(BaseEntityCache *)cache
{
    if(cache) {
        return [self initWithIdentifier:cache.identifier];
    }
    else {
        return nil;
    }
}

- (BaseEntityCache *)cachedObjectWithClient:(CacheDataManager *)cacheClient
{
    if(cacheClient) {
        NSString *entityName = NSStringFromClass([[self class] cachedObjectClass]);
        NSFetchRequest *request = [[NSFetchRequest alloc]initWithEntityName:entityName];
        request.predicate = [NSPredicate predicateWithFormat:@"identifier==%@",self.identifier];
        BaseEntityCache *cache = (BaseEntityCache *)[cacheClient getEntityWithRequest:request];
        if(!cache) {
            cache = [cacheClient createObjectWithType:[[self class]cachedObjectClass]];
        }
        cache.identifier = self.identifier;
        return cache;
    }
    else {
        return nil;
    }
}

- (void)cache:(CacheDataManager *)cacheClient
{
    [self cachedObjectWithClient:cacheClient];
    [cacheClient saveDatabase];
}

@end