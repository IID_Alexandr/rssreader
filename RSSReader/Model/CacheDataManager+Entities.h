//
//  CacheDataManager+Entities.h
//  RSSReader
//
//  Created by Alexandr Chernyshev on 12/06/15.
//  Copyright (c) 2015 Alexandr Chernyshev. All rights reserved.
//

#import "CacheDataManager.h"

#import "CacheRequest.h"

@class BaseEntity;

@interface CacheDataManager (Entities)

- (id)performDatabaseRequestObject:(CacheRequest *)cacheRequest;

- (NSArray *)entitiesWithClass:(Class)aClass;
- (BaseEntity *)entityWithClass:(Class)aClass identifier:(NSString *)identifier;
- (NSArray *)entitiesWithClass:(Class)aClass identifiers:(NSArray *)identifiers;
- (BaseEntity *)entityWithClass:(Class)aClass predicate:(NSPredicate *)predicate;
- (NSArray *)entitiesWithClass:(Class)aClass predicate:(NSPredicate *)predicate;
@end
