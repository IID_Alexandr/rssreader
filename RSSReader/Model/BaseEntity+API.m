//
//  BaseEntity+Parser.m
//  RSSReader
//
//  Created by Alexandr Chernyshev on 23/05/15.
//  Copyright (c) 2015 Alexandr Chernyshev. All rights reserved.
//

#import "BaseEntity+API.h"

#import "NSDictionary+SafeParsing.h"
#import "IDUtils.h"

@implementation BaseEntity (API)

#pragma mark - public methods
+ (NSString *)idKey
{
    return @"id";
}

+ (NSString *)defaultIdentifierWithDictionary:(NSDictionary *)values
{
    return nil;
}

+ (instancetype)objectWithDictionary:(NSDictionary *)values
{
    if(values.count) {
        NSString * identifier = [values stringAtKey:[[self class]idKey]];
        if(!identifier) {
            identifier = [[self class] defaultIdentifierWithDictionary:values];
        }
        id result = [[[self class] alloc] initWithIdentifier:identifier];
        [result updateWithDictionary:values];
        return result;
    }
    else {
        return nil;
    }
}

+ (NSArray *)objectsArrayWithResponse:(NSArray *)response
{
    return [response mapLike:^id(id object) {
        return [[self class] objectWithDictionary:object];
    }];
}

- (void)updateWithDictionary:(NSDictionary *)values
{
    NSAssert(NO, @"Subclass Me");
}

@end
