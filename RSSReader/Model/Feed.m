//
//  Feed.m
//  RSSReader
//
//  Created by Alexandr Chernyshev on 27/11/15.
//  Copyright © 2015 Improve Digital. All rights reserved.
//

#import "Feed.h"

@implementation Feed

- (BOOL)hasImage
{
    return (self.imageURL != nil);
}

- (NSArray *)keywords
{
    NSMutableSet *keywords = [NSMutableSet setWithArray:[self.title componentsSeparatedByString:@" "]];
    NSArray *detailsWords = [self.details componentsSeparatedByString:@" "];
    u_int32_t wordsCount = (u_int32_t)detailsWords.count;
    for (NSInteger i = 0; i < 10; i++) {
        NSInteger index = arc4random_uniform(wordsCount);
        NSString *word = [detailsWords objectAtIndex:index];
        [keywords addObject:word];
    }
    return [keywords allObjects];
}
@end
