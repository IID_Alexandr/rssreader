//
//  HeaderParametersBuilder.m
//  RSSReader
//
//  Created by Alexandr Chernyshev on 29/06/15.
//  Copyright (c) 2015 Improve Digital. All rights reserved.
//

#import "HeaderParametersBuilder.h"

#import "ParametersBuilder+Protected.h"

static NSString * const kContentTypeHeaderKey       = @"Content-Type";
static NSString * const kContentLengthHeaderKey     = @"Content-Length";
static NSString * const kAcceptHeaderKey            = @"Accept";

@implementation HeaderParametersBuilder

+ (instancetype)withContentType:(NSString *)contentType
{
    return [[self builder] withContentType:contentType];
}

+ (instancetype)withAccept:(NSString *)accept
{
    return [[self builder]withAccept:accept];
}

+ (instancetype)withContentLength:(NSInteger)length
{
    return [[self builder] withContentLength:length];
}

- (instancetype)withContentType:(NSString *)contentType
{
    return [self withObject:contentType forKey:kContentTypeHeaderKey];
}

- (instancetype)withAccept:(NSString *)accept
{
    return [self withObject:accept forKey:kAcceptHeaderKey];
}

- (instancetype)withContentLength:(NSInteger)length
{
    return [self withObject:@(length).stringValue forKey:kContentLengthHeaderKey];
}
@end
