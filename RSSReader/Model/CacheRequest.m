//
//  CacheRequest.m
//  RSSReader
//
//  Created by iOS developer on 18/08/15.
//  Copyright (c) 2015 ImproveIT. All rights reserved.
//

#import "CacheRequest.h"

#import "IDUtils.h"

@implementation CacheRequest

- (instancetype)initWithClass:(Class)aClass
                   identifier:(NSString *)identifier
{
    return [self initWithClass:aClass
                     predicate:[NSPredicate predicateWithFormat:@"identifier == %@", identifier]
                          many:NO];
}

- (instancetype)initWithClass:(Class)aClass
                  identifiers:(NSArray *)identifiers
{
    return [self initWithClass:aClass
                     predicate:[NSPredicate predicateWithFormat:@"identifier IN %@", identifiers]
                          many:YES];
}

- (instancetype)initWithClass:(Class)aClass
                         predicate:(NSPredicate *)predicate
                              many:(BOOL)many
{
    self = [self init];
    if (self) {
        _entityName = [aClass className];
        _predicate = predicate;
        _many = many;
    }
    return self;
}
@end
