//
//  RecipesParser.m
//  Chefdaw
//
//  Created by Alexandr Chernyshev on 10/26/13.
//  Copyright (c) 2013 Alexandr Chernyshev. All rights reserved.
//

#import "XMLParser.h"

#import "TBXML.h"

@interface XMLParser()
{
    NSString *      _xmlString;
}

@end

@implementation XMLParser

#pragma mark - initialization
+ (id)parserWithXMLString:(NSString *)xmlString
{
    return [[[self class] alloc]initWithXMLString:xmlString];
}

- (id)initWithXMLString:(NSString *)xmlString
{
    self = [super init];
    if(self) {
        _xmlString = xmlString;
    }
    return self;
}

#pragma mark - public methods
- (NSArray *)parsedItems:(NSError *__autoreleasing*)returnError
{
    NSError * error;
    TBXML *xmlRoot = [[TBXML alloc]initWithXMLString:_xmlString error:&error];
    if(error) {
        xmlRoot = nil;
        (*returnError) = error;
        return nil;
    }
    else {
        NSMutableArray *items = [NSMutableArray array];
        [self handleElement:xmlRoot.rootXMLElement withItemsArray:items];
        (*returnError) = nil;
        return items;
    }
}

#pragma mark - working methods
- (void)parseWithCompletion:(XMLParserResultHandler)handler
{
    NSError * error;
    TBXML *xmlRoot = [[TBXML alloc]initWithXMLString:_xmlString error:&error];
    if(error) {
        xmlRoot = nil;
        if(handler) {
            handler(nil, error);
        }
    }
    else {
        NSMutableArray *items = [NSMutableArray array];
        [self handleElement:xmlRoot.rootXMLElement withItemsArray:items];
        if(handler) {
            handler(items, nil);
        }
    }
}

- (void)handleElement:(TBXMLElement *)element withItemsArray:(NSMutableArray *)itemsArray
{
    do {
        NSString *elementName = [TBXML elementName:element];
        if([elementName isEqualToString:@"item"]) {
            [self handleItem:element withItemsArray:itemsArray];
        }
        
        if(element->firstChild) {
            [self handleElement:element->firstChild withItemsArray:itemsArray];
        }
    }
    while ((element = element->nextSibling));
}

- (void)handleItem:(TBXMLElement *)itemElement withItemsArray:(NSMutableArray *)itemsArray
{
    TBXMLElement *child = itemElement->firstChild;
    
    NSDictionary *mutableDictionary = [NSMutableDictionary dictionary];
    while (child) {
        [mutableDictionary setValue:[TBXML textForElement:child] forKey:[TBXML elementName:child]];
        
        child = child->nextSibling;
    }
    [itemsArray addObject:mutableDictionary];
}

@end
