//
//  SFAPIRequest.h
//  RSSReader
//
//  Created by Alexandr Chernyshev on 24/06/15.
//  Copyright (c) 2015 ImproveIT. All rights reserved.
//

#import "ParametersBuilder.h"

#import "APIBodyItem.h"

typedef NS_ENUM(NSInteger, RequestMethod)
{
    RequestMethodGet,
    RequestMethodPost
};

typedef NS_ENUM(NSInteger, ParametersSerializationType)
{
    ParametersSerializationTypeHeader,
    ParametersSerializationTypeURL,
    ParametersSerializationTypeBody,
    ParametersSerializationTypeJSON
};

typedef NS_ENUM(NSInteger, RequestSecurityType)
{
    RequestSecurityTypeNone,
    RequestSecurityTypeApplication,
    RequestSecurityTypeUser
};

@interface APIRequest : NSObject
@property (nonatomic, assign) RequestSecurityType securityType;
@property (nonatomic, readonly) NSURLRequest *urlRequest;

- (instancetype)initWithMethod:(RequestMethod)method
                       urlPath:(NSString *)urlPath;
- (void)addParametersBuilder:(ParametersBuilder *)parametersBuilder
        forSerializationType:(ParametersSerializationType)serializationType;
- (void)addBodyItem:(APIBodyItem *)bodyItem;

@end
