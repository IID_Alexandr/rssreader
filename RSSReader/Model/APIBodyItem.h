//
//  APIBodyItem.h
//  RSSReader
//
//  Created by Alexandr Chernyshev on 25/07/15.
//  Copyright (c) 2015 Alexandr Chernyshev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APIBodyItem : NSObject
@property (nonatomic, readonly) NSString *name;
@property (nonatomic, readonly) NSData *data;
@property (nonatomic, readonly) NSString *fileName;
@property (nonatomic, readonly) NSString *mimeType;

- (instancetype)initWithJPEGData:(NSData *)data name:(NSString *)name;
- (instancetype)initWithData:(NSData *)data name:(NSString *)name fileName:(NSString *)fileName mimeType:(NSString *)mimeType;
@end
