//
//  Channel.h
//  RSSReader
//
//  Created by Alexandr Chernyshev on 27/11/15.
//  Copyright © 2015 Improve Digital. All rights reserved.
//

#import "BaseEntity.h"

@interface Channel : BaseEntity
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *urlString;

@end
