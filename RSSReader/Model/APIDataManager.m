//
//  SFAPIDataManager.m
//  RSSReader
//
//  Created by Alexandr Chernyshev on 24/06/15.
//  Copyright (c) 2015 ImproveIT. All rights reserved.
//

#import "APIDataManager.h"

#import <ReactiveCocoa/ReactiveCocoa.h>
#import <AFNetworking/AFNetworking.h>

#import "ServiceLayer.h"
#import "Macroses.h"
#import "Constants.h"

@interface APIDataManager()
@property (nonatomic, strong) AFHTTPSessionManager *sessionManager;

@end

@implementation APIDataManager

- (instancetype)init
{
    self = [super init];
    if(self) {
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        _sessionManager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:configuration];
        _sessionManager.responseSerializer = [[AFHTTPResponseSerializer alloc] init];
        _sessionManager.responseSerializer.acceptableStatusCodes = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(200, 202)];
//        NSMutableSet *set = [NSMutableSet setWithSet:_sessionManager.responseSerializer.acceptableContentTypes];
//        [set addObject:@"text/html"];
//        [set addObject:@"application/rss+xml"];
//        _sessionManager.responseSerializer.acceptableContentTypes = set;
    }
    return self;
}

- (RACSignal *)performRequest:(APIRequest *)request
{
    if([ServiceLayer internetAvailable]) {
        return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
            NSURLSessionDataTask *dataTask = [self.sessionManager dataTaskWithRequest:request.urlRequest completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
                if(error) {
                    [subscriber sendError:error];
                }
                else {
                    [subscriber sendNext:responseObject];
                    [subscriber sendCompleted];
                }
            }];
            [dataTask resume];
            return [RACDisposable disposableWithBlock:^{
                [dataTask cancel];
            }];
        }];
    }
    else {
        return [RACSignal error:[NSError errorWithDomain:@"APIDataManagerDomain" code:kNoInternetCode userInfo:@{NSLocalizedDescriptionKey:LOC(@"server.nointernet")}]];
    }
}

@end
