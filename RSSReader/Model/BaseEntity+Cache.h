//
//  BaseEntity+CacheParsing.h
//  RSSReader
//
//  Created by Alexandr Chernyshev on 27/05/15.
//  Copyright (c) 2015 Alexandr Chernyshev. All rights reserved.
//

#import "BaseEntity.h"

#import "BaseEntityCache.h"
#import "CacheProtocol.h"

@class CacheDataManager;

@interface BaseEntity (Cache) <CacheProtocol>

+ (Class)cachedObjectClass;
- (instancetype)initWithCache:(BaseEntityCache *)cache;
- (BaseEntityCache *)cachedObjectWithClient:(CacheDataManager *)cacheClient;
@end
