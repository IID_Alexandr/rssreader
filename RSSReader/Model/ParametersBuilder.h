//
//  ParametersBuilder.h
//  RSSReader
//
//  Created by Alexandr Chernyshev on 10/06/15.
//  Copyright (c) 2015 Alexandr Chernyshev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ParametersBuilder : NSObject

+ (instancetype)withObject:(id)object forKey:(NSString *)key;
+ (instancetype)withParameters:(ParametersBuilder *)parameters;

- (instancetype)withObject:(id)object forKey:(NSString *)key;
- (instancetype)withParameters:(ParametersBuilder *)parameters;

- (NSDictionary *)params;
- (NSData *)jsonParams;
- (NSData *)plainDataParams;
- (NSString *)queryString;
@end
