//
//  SFAPIDataManager.h
//  RSSReader
//
//  Created by Alexandr Chernyshev on 24/06/15.
//  Copyright (c) 2015 ImproveIT. All rights reserved.
//

#import "APIRequest.h"

@class RACSignal;

@interface APIDataManager : NSObject

- (RACSignal *)performRequest:(APIRequest *)request;
@end
