//
//  CacheRequest.h
//  RSSReader
//
//  Created by iOS developer on 18/08/15.
//  Copyright (c) 2015 ImproveIT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CacheRequest : NSObject
@property (nonatomic, copy, readonly) NSString* entityName;
@property (nonatomic, strong, readonly) NSPredicate* predicate;
@property (nonatomic, readonly) BOOL many;

- (instancetype)initWithClass:(Class)aClass
                    predicate:(NSPredicate*)predicate
                         many:(BOOL)many;
- (instancetype)initWithClass:(Class)aClass
                   identifier:(NSString *)identifier;
- (instancetype)initWithClass:(Class)aClass
                  identifiers:(NSArray *)identifiers;
@end
