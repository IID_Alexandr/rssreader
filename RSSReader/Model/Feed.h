//
//  Feed.h
//  RSSReader
//
//  Created by Alexandr Chernyshev on 27/11/15.
//  Copyright © 2015 Improve Digital. All rights reserved.
//

#import "BaseEntity.h"

@interface Feed : BaseEntity
@property (nonatomic, strong) NSString *    title;
@property (nonatomic, strong) NSString *    details;
@property (nonatomic, strong) NSDate   *    date;
@property (nonatomic, strong) NSURL *       url;
@property (nonatomic, strong) NSURL *       imageURL;
@property (nonatomic, strong) NSString *    channelIdentifier;

@property (nonatomic, readonly) NSArray *keywords;

//@property (nonatomic, strong) NSString *    commentsLink;
//@property (nonatomic, strong) NSString *    category;

- (BOOL)hasImage;
@end
