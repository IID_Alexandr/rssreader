//
//  AppDelegate.m
//  RSSReader
//
//  Created by Alexandr Chernyshev on 27/11/15.
//  Copyright © 2015 Improve Digital. All rights reserved.
//

#import "AppDelegate.h"

#import <CoreSpotlight/CoreSpotlight.h>

#import "ServiceLayer.h"
#import "ChannelsService.h"
#import "IDUtils.h"
#import "Macroses.h"
#import "Constants.h"
#import "UIStoryboard+Fabric.h"
#import "UIViewController+IDController.h"
#import "FeedListViewController.h"

@interface AppDelegate ()
@property (nonatomic, readonly) UINavigationController *navController;

@end

@implementation AppDelegate

#pragma mark - UIApplication methods
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [application setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalMinimum];
    [self updateApplicationDynamicShortcuts];
    return YES;
    
//    UIApplicationShortcutItem *shortCutItem = [launchOptions objectForKey:UIApplicationLaunchOptionsShortcutItemKey];
//    return (shortCutItem == nil);
}

- (void)application:(UIApplication *)application performActionForShortcutItem:(UIApplicationShortcutItem *)shortcutItem completionHandler:(void (^)(BOOL))completionHandler
{
    if([shortcutItem.type isEqualToString:kAddNewChannelIdentifier]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:LOC(@"Sorry")
                                                            message:LOC(@"Here should be \"Add channel\" screen")
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil, nil];
        [alertView show];
    }
    else {
        [self openChannelWithIdentifier:shortcutItem.type];
    }
    if(completionHandler) {
        completionHandler(YES);
    }
}

- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray * _Nullable))restorationHandler
{
    if([userActivity.activityType isEqualToString:kChannelUserActivityType]) {
        NSString *channelID = [userActivity.userInfo objectForKey:@"id"];
        [self openChannelWithIdentifier:channelID];
        return YES;
    }
    else if ([userActivity.activityType isEqualToString:CSSearchableItemActionType]) {
        NSString *uniqIdentifier = [userActivity.userInfo objectForKey:CSSearchableItemActivityIdentifier];
        [self openFeedWithIdentifier:uniqIdentifier];
        return YES;
    }
    else {
        return NO;
    }
}

- (void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    __weak typeof(self) weakSelf = self;
    [[[ServiceLayer sharedLayer].channelService loadAllFeeds] subscribeNext:^(id x) {
        FeedListViewController *feedListViewController = (FeedListViewController *)[weakSelf.navController topViewController];
        UIBackgroundFetchResult result;
        if([feedListViewController isMemberOfClass:[FeedListViewController class]]) {
            [feedListViewController updateWithFeedSet:x];
            result = UIBackgroundFetchResultNewData;
        }
        else {
            result = UIBackgroundFetchResultNoData;
        }
        if(completionHandler) {
            completionHandler(result);
        }
    } error:^(NSError *error) {
        if(completionHandler) {
            completionHandler(UIBackgroundFetchResultFailed);
        }
    }];
}

#pragma mark - properties
- (UINavigationController *)navController
{
    return (UINavigationController *)self.window.rootViewController;
}

#pragma mark - working methods
- (void)updateApplicationDynamicShortcuts
{
    [[[ServiceLayer sharedLayer].channelService loadChannels] subscribeNext:^(NSArray *channels) {
        NSArray *cutChannels = [channels subarrayWithRange:NSMakeRange(0, MIN(kMaxNumberOfShortcurChannels, channels.count))];
        [UIApplication sharedApplication].shortcutItems = [cutChannels mapLike:^UIApplicationShortcutItem *(Channel *channel) {
            UIMutableApplicationShortcutItem *item = [[UIMutableApplicationShortcutItem alloc] initWithType:channel.identifier localizedTitle:channel.title];
            item.localizedSubtitle = channel.urlString;
            item.icon = [UIApplicationShortcutIcon iconWithType:UIApplicationShortcutIconTypeFavorite];
            return item;
        }];
    } error:^(NSError *error) {
        [UIApplication sharedApplication].shortcutItems = nil;
    }];
}

- (void)openChannelWithIdentifier:(NSString *)channelIdentifier
{
    UIViewController *listController = [[UIStoryboard mainStoryboard] feedListController];
    [listController connectWithSegue:nil object:channelIdentifier];
    [self showController:listController];
}

- (void)openFeedWithIdentifier:(NSString *)feedIdentifier
{
    UIViewController *detailsController = [[UIStoryboard mainStoryboard] feedDetailsController];
    [detailsController connectWithSegue:nil object:feedIdentifier];
    [self showController:detailsController];
}

- (void)showController:(UIViewController *)viewController
{
    NSMutableArray *viewControllers = [NSMutableArray arrayWithObject:self.navController.viewControllers.firstObject];
    [viewControllers addObject:viewController];
    [self.navController setViewControllers:viewControllers animated:YES];
}

@end
