//
//  NSArray+Cache.m
//  RSSReader
//
//  Created by iOS developer on 18/08/15.
//  Copyright (c) 2015 ImproveIT. All rights reserved.
//

#import "NSArray+Cache.h"

@implementation NSArray (Cache)

- (void)cache:(CacheDataManager *)cacheClient
{
    for (id entity in self) {
        if([entity conformsToProtocol:@protocol(CacheProtocol)]) {
            [entity cache:cacheClient];
        }
    }
    [cacheClient saveDatabase];
}

@end
