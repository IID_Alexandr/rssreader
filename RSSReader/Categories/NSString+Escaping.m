//
//  NSString+Escaping.m
//  RSSReader
//
//  Created by Alexandr Chernyshev on 28/10/14.
//  Copyright (c) 2014 ImproveIT. All rights reserved.
//

#import "NSString+Escaping.h"

@implementation NSString (Escaping)

-(NSString *)oloUrlEncodedString
{
//  return self;
    CFStringRef selfRef = (__bridge CFStringRef)self;
    CFStringRef stringRef = CFURLCreateStringByAddingPercentEscapes(NULL,
                                                                    selfRef,
                                                                    NULL,
                                                                    (CFStringRef)@"!*'\"();@+$,%#[]% ",
                                                                    CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding)
                                                                    );
    NSString *resultString = (__bridge_transfer NSString *)stringRef;
    return resultString;
}
@end
