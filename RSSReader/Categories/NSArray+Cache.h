//
//  NSArray+Cache.h
//  RSSReader
//
//  Created by iOS developer on 18/08/15.
//  Copyright (c) 2015 ImproveIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CacheProtocol.h"

@interface NSArray (Cache) <CacheProtocol>

@end
