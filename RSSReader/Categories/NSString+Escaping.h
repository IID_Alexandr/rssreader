//
//  NSString+Escaping.h
//  RSSReader
//
//  Created by Alexandr Chernyshev on 28/10/14.
//  Copyright (c) 2014 ImproveIT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Escaping)

- (NSString *)oloUrlEncodedString;
@end
