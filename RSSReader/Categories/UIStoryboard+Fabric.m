//
//  UIStoryboard+Fabric.m
//  RSSReader
//
//  Created by Alexandr Chernyshev on 28/11/15.
//  Copyright © 2015 Improve Digital. All rights reserved.
//

#import "UIStoryboard+Fabric.h"

static NSString * const kMainStoryboardName         = @"Main";
static NSString * const kFeedListControllerName     = @"FeedListController";
static NSString * const kFeedDetailsControllerName  = @"FeedDetailsController";

@implementation UIStoryboard (Fabric)

+ (instancetype)mainStoryboard
{
    return [UIStoryboard storyboardWithName:kMainStoryboardName bundle:[NSBundle mainBundle]];
}

- (UIViewController *)feedListController
{
    return [self instantiateViewControllerWithIdentifier:kFeedListControllerName];
}

- (UIViewController *)feedDetailsController
{
    return [self instantiateViewControllerWithIdentifier:kFeedDetailsControllerName];
}
@end
