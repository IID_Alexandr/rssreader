//
//  UIStoryboard+Fabric.h
//  RSSReader
//
//  Created by Alexandr Chernyshev on 28/11/15.
//  Copyright © 2015 Improve Digital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIStoryboard (Fabric)

+ (instancetype)mainStoryboard;
- (UIViewController *)feedListController;
- (UIViewController *)feedDetailsController;
@end
