//
//  UIViewController+RSSController.h
//  RSSReader
//
//  Created by Alexandr Chernyshev on 07/07/15.
//  Copyright (c) 2015 Alexandr Chernyshev. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ViewControllerDataSource.h"
#import "UIViewController+IDController.h"

@class RACSignal;

typedef NS_ENUM(NSInteger, MessageType)
{
    MessageTypeError,
    MessageTypeSuccess,
    MessageTypeInfo
};

@interface UIViewController (RSSController)
@property (nonatomic, assign) BOOL busy;
@property (nonatomic, strong) id<ViewControllerDataSource> presenter;

//Reachability
- (void)onlineControllerMode;
- (void)offlineControllerMode;

//Signal handling
- (void)handleSignal:(RACSignal *)signal;
- (void)initialyLoading;
- (void)handleNext:(id)x;
- (void)handleError:(NSError *)error;
- (void)finalyLoading;
- (void)cancelCurrentOperation;

//Messages
- (void)showMessage:(NSString *)message;
- (void)showMessage:(NSString *)message type:(MessageType)type;
- (void)showMessage:(NSString *)message type:(MessageType)type duration:(CGFloat)duration;
@end
