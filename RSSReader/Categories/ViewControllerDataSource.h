//
//  ViewControllerDataSource.h
//  RSSReader
//
//  Created by Alexandr Chernyshev on 21/06/15.
//  Copyright (c) 2015 Alexandr Chernyshev. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RACSignal;

@protocol ViewControllerDataSource <NSObject>

- (RACSignal *)loadScreenData;
@end
