//
//  UIViewController+RSSController.m
//  RSSReader
//
//  Created by Alexandr Chernyshev on 07/07/15.
//  Copyright (c) 2015 Alexandr Chernyshev. All rights reserved.
//

#import "UIViewController+RSSController.h"

#import <objc/runtime.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

#import "NSObject+SwizzleMethod.h"
#import "ActivityIndicatorView.h"

#import "BaseViewController.h"
#import "BaseTableViewController.h"
#import "ServiceLayer.h"
#import "UIViewController+IDController.h"

static void const * kBusyPropertyKey                = &kBusyPropertyKey;
static void const * kPresenterPropertyKey           = &kPresenterPropertyKey;
static void const * kLastErrorPropertyKey           = &kLastErrorPropertyKey;
static void const * kLastErrorDatePropertyKey       = &kLastErrorDatePropertyKey;
static void const * kActivityIndicatorPropertyKey   = &kActivityIndicatorPropertyKey;
static void const * kCurrentDisposablePropertyKey   = &kCurrentDisposablePropertyKey;

@interface UIViewController (Internal)
@property (nonatomic, strong) RACDisposable *currentDisposable;
@property (nonatomic, strong) NSError *lastError;
@property (nonatomic, strong) NSDate *lastErrorDate;

@end

@implementation UIViewController (RSSController)

#pragma mark - methods swizzling methods
+ (void)load
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [self swizzleSelector:@selector(initWithCoder:) onSelector:@selector(sf_initWithCoder:)];
        [self swizzleSelector:@selector(initWithNibName:bundle:) onSelector:@selector(sf_initWithNibName:bundle:)];
        [self swizzleSelector:@selector(viewDidLoad) onSelector:@selector(sf_viewDidLoad)];
        [self swizzleSelector:@selector(viewWillAppear:) onSelector:@selector(sf_viewWillAppear:)];
        [self swizzleSelector:@selector(viewWillDisappear:) onSelector:@selector(sf_viewWillDisappear:)];
        [self swizzleSelector:@selector(willMoveToParentViewController:) onSelector:@selector(sf_willMoveToParentViewController:)];
    });
}

#pragma mark - Swizzled methods
- (id)sf_initWithCoder:(NSCoder *)aDecoder
{
    id result = [self sf_initWithCoder:aDecoder];
    if([self shouldHandleAction]) {
        if(result) {
            [result updateControllerForParent:self.navigationController];
        }
    }
    return result;
}

- (id)sf_initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    id result = [self sf_initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if([self shouldHandleAction]) {
        if(result) {
            [result updateControllerForParent:self.navigationController];
        }
    }
    return result;
}

- (void)sf_viewDidLoad
{
    [self sf_viewDidLoad];
    
    if([self shouldHandleAction]) {
        [self handleSignal:[self.presenter loadScreenData]];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityStatusChanged:) name:kInternetAvailabilityStatusChangedNotification object:nil];
    }
}

- (void)sf_viewWillAppear:(BOOL)animated
{
    [self sf_viewWillAppear:animated];
    if([self shouldHandleAction]) {
        if(self.busy) {
            [self showActivityView];
        }
        [self reachabilityStatusChanged:nil];
    }
}

- (void)sf_viewWillDisappear:(BOOL)animated
{
    [self sf_viewWillDisappear:animated];
    if([self shouldHandleAction]) {
        [self hideActivityView];
    }
}

- (void)sf_willMoveToParentViewController:(UIViewController *)parent
{
    [self sf_willMoveToParentViewController:parent];
    if([self shouldHandleAction]) {
        [self updateControllerForParent:parent];
    }
}

#pragma mark - properties
- (void)setBusy:(BOOL)busy
{
    if(![NSThread isMainThread]) {
        [self performSelectorOnMainThread:@selector(setBusy:) withObject:@(busy)waitUntilDone:NO];
        return ;
    }
    if(self.busy != busy) {
        objc_setAssociatedObject(self, kBusyPropertyKey, @(busy), OBJC_ASSOCIATION_RETAIN);
        if(busy) {
            [self performSelector:@selector(showActivityView) withObject:nil afterDelay:0.2];
        }
        else {
            [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(showActivityView) object:nil];
            [self hideActivityView];
        }
    }
}

- (BOOL)busy
{
    return ((NSNumber *)objc_getAssociatedObject(self, kBusyPropertyKey)).boolValue;
}

- (ActivityIndicatorView *)activityView
{
    ActivityIndicatorView *actView = objc_getAssociatedObject(self, kActivityIndicatorPropertyKey);
    if(!actView) {
        actView = [[ActivityIndicatorView alloc]initWithFrame:self.view.bounds];
        actView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self.view addSubview:actView];
        [actView finishActivity];
        objc_setAssociatedObject(self, kActivityIndicatorPropertyKey, actView, OBJC_ASSOCIATION_ASSIGN);
    }
    return actView;
}

- (void)setLastError:(NSError *)lastError
{
    objc_setAssociatedObject(self, kLastErrorPropertyKey, lastError, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSError *)lastError
{
    return objc_getAssociatedObject(self, kLastErrorPropertyKey);
}

- (void)setLastErrorDate:(NSDate *)lastErrorDate
{
    objc_setAssociatedObject(self, kLastErrorDatePropertyKey, lastErrorDate, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSDate *)lastErrorDate
{
    NSDate *date = objc_getAssociatedObject(self, kLastErrorDatePropertyKey);
    if(!date) {
        date = [NSDate dateWithTimeIntervalSince1970:0];
    }
    return date;
}

- (void)setPresenter:(id<ViewControllerDataSource>)presenter
{
    objc_setAssociatedObject(self, kPresenterPropertyKey, presenter, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (id<ViewControllerDataSource>)presenter
{
    return objc_getAssociatedObject(self, kPresenterPropertyKey);
}

- (void)setCurrentDisposable:(RACDisposable *)currentDisposable
{
    objc_setAssociatedObject(self, kCurrentDisposablePropertyKey, currentDisposable, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (RACDisposable *)currentDisposable
{
    return objc_getAssociatedObject(self, kCurrentDisposablePropertyKey);
}

#pragma mark - public methods
- (void)initialyLoading
{
    self.busy = YES;
}

- (void)handleNext:(id)x
{
}

- (void)handleError:(NSError *)error
{
    NSDate *currentDate = [NSDate date];
    CGFloat seconds = [currentDate timeIntervalSinceDate:self.lastErrorDate];
    if(![self error:self.lastError isEqualToError:error] || seconds > 3) {
        self.lastError = error;
        self.lastErrorDate = currentDate;
        [self showMessage:error.localizedDescription type:MessageTypeError];
    }
}

- (void)finalyLoading
{
    self.busy = NO;
    [self cancelCurrentOperation];
}

- (void)cancelCurrentOperation
{
    [self.currentDisposable dispose];
    self.currentDisposable = nil;
}

- (void)handleSignal:(RACSignal *)signal
{
    @weakify(self);
    self.currentDisposable = [[[signal initially:^{
        @strongify(self);
        [self initialyLoading];
    }] finally:^{
        @strongify(self);
        [self finalyLoading];
    }] subscribeNext:^(id x) {
        @strongify(self);
        [self handleNext:x];
    } error:^(NSError *error) {
        @strongify(self);
        [self handleError:error];
    }];
}

- (void)showMessage:(NSString *)message
{
    [self showMessage:message type:MessageTypeInfo duration:0.25];
}

- (void)showMessage:(NSString *)message type:(MessageType)type
{
    [self showMessage:message type:type duration:0.25];
}

- (void)showMessage:(NSString *)message type:(MessageType)type duration:(CGFloat)duration
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil, nil];
    [alertView show];
}

#pragma mark - working methods
- (BOOL)shouldHandleAction
{
    return ([self isKindOfClass:[BaseViewController class]] || [self isKindOfClass:[BaseTableViewController class]]);
}

- (BOOL)error:(NSError *)first isEqualToError:(NSError *)second
{
    return ((first.code == second.code) && [first.domain isEqualToString:second.domain]);
}

- (void)updateControllerForParent:(UIViewController *)parent
{
    if([parent isKindOfClass:[UINavigationController class]]) {
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    }
}

#pragma mark - Activiry Logic
- (void)showActivityView
{
    [self.activityView.superview bringSubviewToFront:self.activityView];
    [self.activityView startActivity:self.view];
    self.view.userInteractionEnabled = NO;
}

- (void)hideActivityView
{
    [self.activityView finishActivity];
    self.view.userInteractionEnabled = YES;
}


#pragma mark - Reachability Methods
- (void)reachabilityStatusChanged:(NSNotification *)notification
{
    if([ServiceLayer internetAvailable]) {
        [self onlineControllerMode];
    }
    else {
        [self offlineControllerMode];
    }
}

- (void)onlineControllerMode
{
}

- (void)offlineControllerMode
{
}

@end
