//
//  BaseItem_Protected.h
//  RSSReader
//
//  Created by Alexandr Chernyshev on 24.07.15.
//  Copyright (c) 2015 ImproveIT. All rights reserved.
//

#import "BaseItem.h"

@interface BaseItem ()
@property (nonatomic, strong) id object;

@end
