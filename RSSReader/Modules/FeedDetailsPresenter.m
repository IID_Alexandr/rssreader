//
//  FeedDetailsPresenter.m
//  RSSReader
//
//  Created by Alexandr Chernyshev on 28/11/15.
//  Copyright © 2015 Improve Digital. All rights reserved.
//

#import "FeedDetailsPresenter.h"

#import <ReactiveCocoa/ReactiveCocoa.h>

#import "ViewControllerDataSource.h"
#import "ServiceLayer.h"
#import "ChannelsService.h"

@interface FeedDetailsPresenter() <ViewControllerDataSource>
@property (nonatomic, strong) Feed *feed;
@property (nonatomic, strong) NSString *feedIdentifier;

@end

@implementation FeedDetailsPresenter

#pragma mark - initialization
- (instancetype)initWithObject:(id)object
{
    self = [super init];
    if(self) {
        if([object isKindOfClass:[Feed class]]) {
            self.feed = object;
        }
        else {
            _feedIdentifier = object;
        }
    }
    return self;
}

#pragma mark - properties
- (NSURL *)url
{
    return self.feed.url;
}

#pragma mark - <ViewControllerDataSource> methods
- (RACSignal *)loadScreenData
{
    if(!self.feed) {
        __weak typeof(self) weakSelf = self;
        return [[[ServiceLayer sharedLayer].channelService loadFeedWithIdentifier:self.feedIdentifier] doNext:^(Feed *feed) {
            weakSelf.feed = feed;
        }];
    }
    else {
        return [RACSignal empty];
    }
}

#pragma mark - KVO
+ (NSSet *)keyPathsForValuesAffectingUrl
{
    return [NSSet setWithObjects:@"feed", nil];
}

@end
