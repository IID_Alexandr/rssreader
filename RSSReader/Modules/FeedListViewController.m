//
//  FeedListViewController.m
//  RSSReader
//
//  Created by Alexandr Chernyshev on 27/11/15.
//  Copyright © 2015 Improve Digital. All rights reserved.
//

#import "FeedListViewController.h"

#import <ReactiveCocoa/ReactiveCocoa.h>

#import "FeedListPresenter.h"
#import "UIViewController+RSSController.h"
#import "UIViewController+IDController.h"
#import "Feed.h"
#import "IDUtils.h"
#import "FeedTableViewCell.h"

@interface FeedListViewController()
@property (nonatomic, strong) FeedListPresenter *presenter;

@end

@implementation FeedListViewController
@dynamic presenter;

#pragma mark - lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self registerNibForClass:[FeedTableViewCell class]];
    [self.reuseIdentifierMatcher registerReuseIdentifier:[FeedTableViewCell className] forItemClass:[Feed class]];
}

- (void)updateUserActivityState:(NSUserActivity *)activity
{
    [activity addUserInfoEntriesFromDictionary:self.presenter.activityUserInfo];
}

#pragma mark - public methods
- (void)updateWithFeedSet:(NSDictionary *)feedSet
{
    [self.presenter updateWithFeedSet:feedSet];
}

#pragma mark - standard methods
- (void)racBinding
{
    [super racBinding];
    RAC(self, title) = RACObserve(self, presenter.title);
    RAC(self, userActivity) = [RACObserve(self, presenter.userActivity) doNext:^(NSUserActivity *userActivity) {
        userActivity.eligibleForSearch = YES;
    }];
}

#pragma mark - UITableViewCell handling
- (CGFloat)rowHeightForItem:(id)item
{
    return kIDDynamicRowHeight;
}

#pragma mark - transitions
- (void)connectWithSegue:(UIStoryboardSegue *)segue object:(id)object
{
    self.presenter = [[FeedListPresenter alloc] initWithObject:object];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [segue.destinationViewController connectWithSegue:segue object:sender];
}
@end
