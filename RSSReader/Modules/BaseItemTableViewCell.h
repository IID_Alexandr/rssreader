//
//  BaseItemTableViewCell.h
//  RSSReader
//
//  Created by Alexandr Chernyshev on 27.07.15.
//  Copyright (c) 2015 ImproveIT. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "IDViewCell.h"

@interface BaseItemTableViewCell : UITableViewCell <IDViewCell>

@end
