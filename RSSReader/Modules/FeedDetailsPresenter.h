//
//  FeedDetailsPresenter.h
//  RSSReader
//
//  Created by Alexandr Chernyshev on 28/11/15.
//  Copyright © 2015 Improve Digital. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FeedDetailsPresenter : NSObject
@property (nonatomic, readonly) NSURL *url;

- (instancetype)initWithObject:(id)object;
@end
