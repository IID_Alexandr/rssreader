//
//  ChannelsListViewController.m
//  RSSReader
//
//  Created by Alexandr Chernyshev on 27/11/15.
//  Copyright © 2015 Improve Digital. All rights reserved.
//

#import "ChannelsListViewController.h"

#import "IDUtils.h"
#import "Channel.h"
#import "UIViewController+IDController.h"
#import "ChannelsListPresenter.h"
#import "Macroses.h"

@implementation ChannelsListViewController

#pragma mark - initialization
- (void)customInitialize
{
    self.presenter = [[ChannelsListPresenter alloc] init];
}

#pragma mark - lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = LOC(@"Channels");
    [self.reuseIdentifierMatcher registerReuseIdentifier:[UITableViewCell className] forItemClass:[Channel class]];
}

#pragma mark - UITableViewDataSource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    if (!cell) {
        id item = [self.presenter itemAtIndexPath:indexPath];
        NSString *reuseIdentifier = [self.reuseIdentifierMatcher reuseIdentifierForItem:item];
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier];
        [self configureCell:(UITableViewCell <IDViewCell> *)cell forItem:item];
    }
    return cell;
}

#pragma mark - UITableViewCell handling
- (CGFloat)rowHeightForItem:(id)item
{
    return 44.0f;
}

- (void)configureCell:(UITableViewCell<IDViewCell> *)cell forItem:(id<IDBaseItemProtocol>)item
{
    Channel *channel = (Channel *)item;
    cell.textLabel.text = channel.title;
    cell.detailTextLabel.text = channel.urlString;
}

#pragma mark - transition
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [segue.destinationViewController connectWithSegue:segue object:sender];
}
@end
