//
//  FeedListPresenter.m
//  RSSReader
//
//  Created by Alexandr Chernyshev on 27/11/15.
//  Copyright © 2015 Improve Digital. All rights reserved.
//

#import "FeedListPresenter.h"

#import <CoreSpotlight/CoreSpotlight.h>
#import <MobileCoreServices/MobileCoreServices.h>

#import "ViewControllerDataSource.h"
#import "ServiceLayer.h"
#import "ChannelsService.h"
#import "IDUtils.h"
#import "Constants.h"

@interface FeedListPresenter() <ViewControllerDataSource>
@property (nonatomic, strong) Channel *channel;
@property (nonatomic, strong) NSString *channelIdentifier;
@property (nonatomic, readwrite, strong) NSUserActivity *userActivity;

@end

@implementation FeedListPresenter

+ (BOOL)allowEditing
{
    return YES;
}

#pragma mark - initialization
- (instancetype)initWithObject:(id)object
{
    self = [super initWithRows:[NSMutableArray array]];
    if(self) {
        if([object isKindOfClass:[Channel class]]) {
            self.channel = object;
        }
        else {
            self.channelIdentifier = object;
        }
    }
    return self;
}

#pragma mark - public methods
- (void)updateWithFeedSet:(NSDictionary *)feedSet
{
    NSArray *feeds = [feedSet objectForKey:self.channelIdentifier];
    [self updateRows:feeds];
}

#pragma mark - properties
- (NSString *)title
{
    return self.channel.title;
}

- (NSDictionary *)activityUserInfo
{
    return @{@"id" : self.channel.identifier};
}

- (void)setChannel:(Channel *)channel
{
    _channel = channel;
    self.channelIdentifier = channel.identifier;
    
    NSUserActivity *userActivity = [[NSUserActivity alloc] initWithActivityType:kChannelUserActivityType];
    userActivity.title = channel.title;
    userActivity.userInfo = self.activityUserInfo;
    userActivity.keywords = [NSSet setWithArray:[self.title componentsSeparatedByString:@" "]];
    
    CSSearchableItemAttributeSet *attribureSet = [[CSSearchableItemAttributeSet alloc] initWithItemContentType:(NSString *)kUTTypeData];
    attribureSet.title = channel.title;
    attribureSet.contentDescription = channel.urlString;
    attribureSet.keywords = userActivity.keywords.allObjects;
    userActivity.contentAttributeSet = attribureSet;
        
    self.userActivity = userActivity;
}

#pragma mark - <ViewControllerDataSource> methods
- (RACSignal *)loadScreenData
{
    __weak typeof (self) weakSelf = self;
    if(!self.channel) {
        return [[[ServiceLayer sharedLayer].channelService loadChannelWithIdentifier:self.channelIdentifier] flattenMap:^RACStream *(id value) {
            weakSelf.channel = value;
            return [weakSelf loadChannelFeedsSignal];
        }];
    }
    else {
        return [self loadChannelFeedsSignal];
    }
}

#pragma mark - protected methods
- (void)didSelectItemAtIndexPath:(NSIndexPath *)indexPath completionResult:(void (^)(NSString *, id))completion
{
    id item = [self itemAtIndexPath:indexPath];
    NSString *segueIdentifier = @"ShowFeedDetailsSegue";
    if(completion) {
        completion(segueIdentifier, item);
    }
}

#pragma mark - working methods
- (RACSignal *)loadChannelFeedsSignal
{
    __weak typeof (self) weakSelf = self;
    return [[[ServiceLayer sharedLayer].channelService loadFeedsForChannel:self.channel] doNext:^(NSArray *result) {
        [weakSelf insertItems:result];
    }];
}

#pragma mark - KVO 
+ (NSSet *)keyPathsForValuesAffectingActivityUserInfo
{
    return [NSSet setWithObjects:@"channel", nil];
}

@end
