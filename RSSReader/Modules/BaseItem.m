//
//  BaseItem.m
//  RSSReader
//
//  Created by Alexandr Chernyshev on 24.07.15.
//  Copyright (c) 2015 ImproveIT. All rights reserved.
//

#import "BaseItem.h"

#import "BaseItem+Protected.h"
#import "IDUtils.h"

@implementation BaseItem

#pragma mark - <IDBaseItemProtocol> methods
- (instancetype)initWithObject:(id)object
{
    self = [super init];
    if(self) {
        self.object = object;
    }
    return self;
}

+ (instancetype)itemFromObject:(id)object
{
    return [[[self class] alloc] initWithObject: object];
}

+ (NSArray*)itemsFromObjects:(NSArray*)objects
{
    return [objects mapLike:^id(id object) {
        return [[self class] itemFromObject:object];
    }];
}
@end
