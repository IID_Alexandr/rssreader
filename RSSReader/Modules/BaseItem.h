//
//  BaseItem.h
//  RSSReader
//
//  Created by Alexandr Chernyshev on 24.07.15.
//  Copyright (c) 2015 ImproveIT. All rights reserved.
//

#import "IDBaseItemProtocol.h"

@interface BaseItem : NSObject <IDBaseItemProtocol>

@end
