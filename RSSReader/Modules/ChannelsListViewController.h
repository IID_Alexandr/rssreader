//
//  ChannelsListViewController.h
//  RSSReader
//
//  Created by Alexandr Chernyshev on 27/11/15.
//  Copyright © 2015 Improve Digital. All rights reserved.
//

#import "BaseTableViewController.h"

@interface ChannelsListViewController : BaseTableViewController

@end
