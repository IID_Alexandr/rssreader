//
//  FeedTableViewCell.m
//  RSSReader
//
//  Created by Alexandr Chernyshev on 27/11/15.
//  Copyright © 2015 Improve Digital. All rights reserved.
//

#import "FeedTableViewCell.h"

#import <ReactiveCocoa/ReactiveCocoa.h>
#import <SDWebImage/UIImageView+WebCache.h>

#import "UIView+IDView.h"
#import "Feed.h"

static CGFloat const kDefaultFeedImageSize  = 70;
static CGFloat const kDefaultFeedTextOffset = 10;

@interface FeedTableViewCell()
@property (nonatomic, weak) IBOutlet UIImageView *imgView;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *detailsLabel;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *imageSize;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *textOffset;

@property (nonatomic, strong) Feed *item;

@end

@implementation FeedTableViewCell
@dynamic item;

#pragma mark - binding
- (void)racBinding
{
    [super racBinding];
    RAC(self, titleLabel.text) = RACObserve(self, item.title);
    RAC(self, detailsLabel.text) = RACObserve(self, item.details);

    __weak typeof(self) weakSelf = self;
    [RACObserve(self, item.imageURL) subscribeNext:^(NSURL *x) {
        if(x) {
            [weakSelf.imgView sd_setImageWithURL:x];
            weakSelf.imageSize.constant = kDefaultFeedImageSize;
            weakSelf.textOffset.constant = kDefaultFeedTextOffset;
        }
        else {
            weakSelf.imageSize.constant = 0;
            weakSelf.textOffset.constant = 0;
        }
        [weakSelf layoutIfNeeded];
    }];
}

@end
