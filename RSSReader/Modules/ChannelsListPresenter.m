//
//  ChannelsListPresenter.m
//  RSSReader
//
//  Created by Alexandr Chernyshev on 27/11/15.
//  Copyright © 2015 Improve Digital. All rights reserved.
//

#import "ChannelsListPresenter.h"

#import "ViewControllerDataSource.h"
#import "ServiceLayer.h"
#import "ChannelsService.h"

@interface ChannelsListPresenter() <ViewControllerDataSource>

@end

@implementation ChannelsListPresenter

- (RACSignal *)loadScreenData
{
    __weak typeof (self) weakSelf = self;
    return [[[ServiceLayer sharedLayer].channelService loadChannels] doNext:^(NSArray *channels) {
        [weakSelf updateRows:channels];
    }];
}

- (void)didSelectItemAtIndexPath:(NSIndexPath *)indexPath completionResult:(void (^)(NSString *, id))completion
{
    id item = [self itemAtIndexPath:indexPath];
    NSString *segueIdentifier = @"ShowFeedsSegue";
    if(completion) {
        completion(segueIdentifier, item);
    }
}
@end
