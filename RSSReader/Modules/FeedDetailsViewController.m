//
//  FeedDetailsViewController.m
//  RSSReader
//
//  Created by Alexandr Chernyshev on 28/11/15.
//  Copyright © 2015 Improve Digital. All rights reserved.
//

#import "FeedDetailsViewController.h"

#import <ReactiveCocoa/ReactiveCocoa.h>

#import "UIViewController+IDController.h"
#import "UIViewController+RSSController.h"
#import "FeedDetailsPresenter.h"
#import "Macroses.h"

@interface FeedDetailsViewController () <UIWebViewDelegate>
@property (nonatomic, weak) IBOutlet UIWebView *webView;
@property (nonatomic, strong) FeedDetailsPresenter *presenter;

@end

@implementation FeedDetailsViewController

#pragma mark - lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = LOC(@"Web");
}

#pragma mark - standard methods
- (void)racBinding
{
    [super racBinding];
    __weak typeof (self) weakSelf = self;
    [RACObserve(self, presenter.url) subscribeNext:^(NSURL *url) {
        [weakSelf.webView loadRequest:[NSURLRequest requestWithURL:url]];
    }];
}

#pragma mark - transitions
- (void)connectWithSegue:(UIStoryboardSegue *)segue object:(id)object
{
    self.presenter = [[FeedDetailsPresenter alloc] initWithObject:object];
}

#pragma mark - <UIWebViewDelegate> methods
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    return [request.URL isEqual:self.presenter.url];
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [self initialyLoading];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self finalyLoading];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(nullable NSError *)error
{
    [self finalyLoading];
    [self handleError:error];
}

@end
