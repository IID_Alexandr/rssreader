//
//  FeedListPresenter.h
//  RSSReader
//
//  Created by Alexandr Chernyshev on 27/11/15.
//  Copyright © 2015 Improve Digital. All rights reserved.
//

#import "IDSingleSectionPresenter.h"

@interface FeedListPresenter : IDSingleSectionPresenter
@property (nonatomic, readonly) NSString *title;
@property (nonatomic, readonly) NSUserActivity *userActivity;
@property (nonatomic, readonly) NSDictionary *activityUserInfo;

- (instancetype)initWithObject:(id)object;
- (void)updateWithFeedSet:(NSDictionary *)feedSet;
@end
