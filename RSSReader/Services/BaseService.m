//
//  BaseService.m
//  PrivateGuide
//
//  Created by Alexandr Chernyshev on 06/06/15.
//  Copyright (c) 2015 Alexandr Chernyshev. All rights reserved.
//

#import "BaseService.h"

#import <ReactiveCocoa/ReactiveCocoa.h>

#import "BaseService+Protected.h"
#import "BaseEntity.h"
#import "NSObject+SafeParsing.h"
#import "BaseEntity+Cache.h"
#import "NSArray+Cache.h"

@implementation BaseService

#pragma mark - initialization
- (instancetype)init
{
    self = [super init];
    if(self) {
        _apiClient = [self apiDataManager];
        _cacheClient = [self cacheDataManager];
    }
    return self;
}

- (APIDataManager *)apiDataManager
{
    APIDataManager *dataManager = [APIDataManager new];
    return dataManager;
}

- (CacheDataManager *)cacheDataManager
{
    return [[CacheDataManager alloc] initWithCacheName:@"RSSReader"];
}

- (RACSignal *)performAPIRequest:(APIRequest *)apiRequest entityClass:(Class)aClass many:(BOOL)many
{
    CacheRequest *cacheRequest = [[CacheRequest alloc]initWithClass:aClass predicate:nil many:many];
    return [self performAPIRequest:apiRequest cacheRequest:cacheRequest map:^id(id response) {
        if(many) {
            return [aClass objectsArrayWithResponse:[response safeArrayValue]];
        }
        else {
            return [aClass objectWithDictionary:[response safeDictionaryValue]];
        }
    }];
}

- (RACSignal *)performAPIRequest:(APIRequest *)apiRequest map:(id(^)(id response))mapBlock
{
    return [self performAPIRequest:apiRequest cacheRequest:nil map:mapBlock];
}

- (RACSignal *)performAPIRequest:(APIRequest *)apiRequest entityClass:(Class)aClass identifier:(NSString *)identifier
{
    CacheRequest *cacheRequest = [[CacheRequest alloc]initWithClass:aClass identifier:identifier];
    return [self performAPIRequest:apiRequest cacheRequest:cacheRequest map:^id(id response) {
        return [aClass objectWithDictionary:[response safeDictionaryValue]];
    }];
}

- (RACSignal *)performAPIRequest:(APIRequest *)apiRequest entityClass:(Class)aClass identifiers:(NSArray *)identifiers
{
    CacheRequest *cacheRequest = [[CacheRequest alloc]initWithClass:aClass identifiers:identifiers];
    return [self performAPIRequest:apiRequest cacheRequest:cacheRequest map:^id(id response) {
        return [aClass objectsArrayWithResponse:[response safeArrayValue]];
    }];
}

- (RACSignal *)performAPIRequest:(APIRequest *)apiRequest cacheRequest:(CacheRequest *)cacheRequest map:(id (^)(id))mapBlock
{
    return [[[self.apiClient performRequest:apiRequest] map:^id(id value) {
        if(mapBlock) {
            id result = mapBlock(value);
            if(result && cacheRequest) {
                //cache here
                if([result conformsToProtocol:@protocol(CacheProtocol)]) {
                    [result cache:self.cacheClient];
                }
            }
            return result;
        }
        else {
            return value;
        }
    }] catch:^RACSignal *(NSError *error) {
        if(cacheRequest) {
            id cache = [self.cacheClient performDatabaseRequestObject:cacheRequest];
            if(cache) {
                return [RACSignal return:cache];
            }
        }
        return [RACSignal error:error];
    }];
}

@end
