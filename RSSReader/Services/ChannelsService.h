//
//  ChannelsService.h
//  RSSReader
//
//  Created by Alexandr Chernyshev on 27/11/15.
//  Copyright © 2015 Improve Digital. All rights reserved.
//

#import "BaseService.h"

#import "Channel.h"
#import "Feed.h"

@interface ChannelsService : BaseService
;
- (RACSignal *)loadChannels;
- (RACSignal *)loadChannelWithIdentifier:(NSString *)channelIdentifier;
- (RACSignal *)loadFeedsForChannel:(Channel *)channel;
- (RACSignal *)loadFeedWithIdentifier:(NSString *)feedIdentifier;
- (RACSignal *)loadAllFeeds;
- (RACSignal *)loadLastFeedsWithChannelsCached;
@end
