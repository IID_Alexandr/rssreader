//
//  BaseService_Base.h
//  PrivateGuide
//
//  Created by Alexandr Chernyshev on 06/06/15.
//  Copyright (c) 2015 Alexandr Chernyshev. All rights reserved.
//

#import "BaseService.h"

#import "APIDataManager.h"
#import "CacheDataManager.h"

@interface BaseService()
@property (nonatomic, readonly) APIDataManager *        apiClient;
@property (nonatomic, readonly) CacheDataManager *      cacheClient;

- (APIDataManager *)apiDataManager;
- (CacheDataManager *)cacheDataManager;

- (RACSignal *)performAPIRequest:(APIRequest *)apiRequest map:(id(^)(id response))mapBlock;
- (RACSignal *)performAPIRequest:(APIRequest *)apiRequest entityClass:(Class)aClass many:(BOOL)many;
- (RACSignal *)performAPIRequest:(APIRequest *)apiRequest entityClass:(Class)aClass identifier:(NSString *)identifier;
- (RACSignal *)performAPIRequest:(APIRequest *)apiRequest entityClass:(Class)aClass identifiers:(NSArray *)identifiers;
- (RACSignal *)performAPIRequest:(APIRequest *)apiRequest cacheRequest:(CacheRequest *)cacheRequest map:(id (^)(id))mapBlock;
@end