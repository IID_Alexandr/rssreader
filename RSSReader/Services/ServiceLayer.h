//
//  ServiceLayer.h
//  RSSReader
//
//  Created by Alexandr Chernyshev on 31/05/15.
//  Copyright (c) 2015 Alexandr Chernyshev. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ChannelsService;

extern NSString * const kInternetAvailabilityStatusChangedNotification;

@interface ServiceLayer : NSObject
@property (nonatomic, readonly) ChannelsService *channelService;

+ (instancetype)sharedLayer;
+ (BOOL)internetAvailable;
@end
