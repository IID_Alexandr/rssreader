//
//  BaseService.h
//  PrivateGuide
//
//  Created by Alexandr Chernyshev on 06/06/15.
//  Copyright (c) 2015 Alexandr Chernyshev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface BaseService : NSObject
@property (nonatomic, readonly) BOOL internetAvailable;

@end
