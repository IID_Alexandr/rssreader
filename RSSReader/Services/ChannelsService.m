//
//  ChannelsService.m
//  RSSReader
//
//  Created by Alexandr Chernyshev on 27/11/15.
//  Copyright © 2015 Improve Digital. All rights reserved.
//

#import "ChannelsService.h"

#import <CoreSpotlight/CoreSpotlight.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <SDWebImage/SDImageCache.h>

#import "BaseService+Protected.h"
#import "APIRequest.h"
#import "XMLDataManager.h"
#import "NSDictionary+SafeParsing.h"
#import "IDUtils.h"
#import "NSArray+Cache.h"
#import "Constants.h"

@implementation ChannelsService

#pragma mark - properties
- (APIDataManager *)apiDataManager
{
    return [XMLDataManager new];
}

#pragma mark - public methods
- (RACSignal *)loadChannels
{
    CacheRequest *cacheRequest = [[CacheRequest alloc]initWithClass:[Channel class] predicate:nil many:YES];
    NSArray *resultChannels = [self.cacheClient performDatabaseRequestObject:cacheRequest];
    if(!resultChannels) {
        Channel *fcunited = [[Channel alloc] initWithIdentifier:@"123123123"];
        fcunited.title = @"United of Manchester";
        fcunited.urlString = @"http://fcunited.ru/rss/";
        Channel *lenta = [[Channel alloc] initWithIdentifier:@"456456456"];
        lenta.title = @"Lenta.ru";
        lenta.urlString = @"http://www.lenta.ru/rss";
        Channel *rt = [[Channel alloc] initWithIdentifier:@"789789789"];
        rt.title = @"Russia Today";
        rt.urlString = @"https://www.rt.com/rss/";
        
        resultChannels = @[fcunited, lenta, rt];
        [resultChannels cache:self.cacheClient];
    }
    return [RACSignal return:resultChannels];
}

- (RACSignal *)loadChannelWithIdentifier:(NSString *)channelIdentifier
{
    CacheRequest *cacheRequest = [[CacheRequest alloc]initWithClass:[Channel class] identifier:channelIdentifier];
    id result = [self.cacheClient performDatabaseRequestObject:cacheRequest];
    if(result) {
        return [RACSignal return:result];
    }
    else {
        return [RACSignal error:[NSError errorWithDomain:@"ChannelServideDomain"
                                                    code:0
                                                userInfo:@{NSLocalizedDescriptionKey : @"Channel not found"}]];
    }
}

- (RACSignal *)loadFeedsForChannel:(Channel *)channel
{
    APIRequest *request = [[APIRequest alloc] initWithMethod:RequestMethodGet urlPath:channel.urlString];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"channelIdentifier == %@", channel.identifier];
    CacheRequest *cacheRequest = [[CacheRequest alloc]initWithClass:[Feed class] predicate:predicate many:YES];
    return [self performAPIRequest:request cacheRequest:cacheRequest map:^id(id response) {
        
        NSMutableArray *searchItems = [NSMutableArray array];
        NSArray *feedArray = [response mapLike:^id(id object) {
            Feed *feed = [[Feed class] objectWithDictionary:object];
            feed.channelIdentifier = channel.identifier;
            
            CSSearchableItemAttributeSet *attribureSet = [[CSSearchableItemAttributeSet alloc] initWithItemContentType:(NSString *)kUTTypeData];
            attribureSet.title = feed.title;
            attribureSet.thumbnailURL = feed.url;
            attribureSet.contentDescription = [NSString stringWithFormat:@"%@\n%@", channel.title, feed.details];
            attribureSet.keywords = feed.keywords;
            
            CSSearchableItem *searchItem = [[CSSearchableItem alloc]initWithUniqueIdentifier:feed.identifier
                                                                            domainIdentifier:kFeedSearchDomainType
                                                                                attributeSet:attribureSet];
            [searchItems addObject:searchItem];
            
            return feed;
        }];
        
        [[CSSearchableIndex defaultSearchableIndex]indexSearchableItems:searchItems completionHandler:^(NSError * _Nullable error) {
            NSLog(@"Index With Error: %@", error);
        }];
        return feedArray;
    }];
}

- (RACSignal *)loadFeedWithIdentifier:(NSString *)feedIdentifier
{
    CacheRequest *cacheRequest = [[CacheRequest alloc]initWithClass:[Feed class] identifier:feedIdentifier];
    id result = [self.cacheClient performDatabaseRequestObject:cacheRequest];
    if(result) {
        return [RACSignal return:result];
    }
    else {
        return [RACSignal error:[NSError errorWithDomain:@"ChannelServideDomain"
                                                    code:0
                                                userInfo:@{NSLocalizedDescriptionKey : @"Feed not found"}]];
    }
}

- (RACSignal *)loadAllFeeds
{
    __weak typeof(self) weakSelf = self;
    return [[[self loadChannels] flattenMap:^(NSArray *channels) {
        NSArray *signalsForEveryChannel = [channels mapLike:^RACSignal *(Channel *channel) {
            return [weakSelf loadFeedsForChannel:channel];
        }];
        return [RACSignal combineLatest:signalsForEveryChannel];
    }] map:^NSDictionary *(RACTuple *feeds) {
        NSArray *feedSets = [feeds allObjects];
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        for(NSArray *feedSet in feedSets) {
            Feed *feed = [feedSet firstObject];
            [dict setObject:feedSet forKey:feed.channelIdentifier];
        }
        return dict;
    }];
}

- (RACSignal *)loadLastFeedsWithChannelsCached
{
    __weak typeof(self) weakSelf = self;
    return [[self loadChannels] map:^id(NSArray *channels) {
        NSArray *feeds = [channels mapLike:^RACSignal *(Channel *channel) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"channelIdentifier == %@", channel.identifier];
            CacheRequest *cacheRequest = [[CacheRequest alloc]initWithClass:[Feed class] predicate:predicate many:NO];
            return [weakSelf.cacheClient performDatabaseRequestObject:cacheRequest];
        }];
        return feeds;
    }];
}

@end
