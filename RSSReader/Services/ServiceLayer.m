//
//  ServiceLayer.m
//  RSSReader
//
//  Created by Alexandr Chernyshev on 31/05/15.
//  Copyright (c) 2015 Alexandr Chernyshev. All rights reserved.
//

#import "ServiceLayer.h"

#import <ReactiveCocoa/ReactiveCocoa.h>
#import <Reachability/Reachability.h>

#import "ChannelsService.h"

NSString * const kInternetAvailabilityStatusChangedNotification = @"InternetAvailabilityStatusChangedNotification";

@interface ServiceLayer()
@property (nonatomic, readonly) Reachability *internetReachability;
@property (nonatomic, assign) BOOL internetAvailable;

@end

@implementation ServiceLayer

#pragma mark - singleton methods
+ (instancetype)sharedLayer
{
    static dispatch_once_t onceToken;
    static ServiceLayer *sharedServiceInstance = nil;
    dispatch_once(&onceToken, ^{
        sharedServiceInstance = [[[self class] alloc]init];
    });
    return sharedServiceInstance;
}

+ (BOOL)internetAvailable
{
    return ((ServiceLayer *)[self sharedLayer]).internetAvailable;
}

#pragma mark - initialization
- (instancetype)init
{
    self = [super init];
    if(self) {
        _channelService = [[ChannelsService alloc] init];
        
        Reachability *reach = [Reachability reachabilityForInternetConnection];
        [reach startNotifier];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
        _internetReachability = reach;
        [self reachabilityChanged:nil];
    }
    return self;
}

- (void)dealloc
{
    [_internetReachability stopNotifier];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
}

#pragma mark - Notification
- (void)reachabilityChanged:(NSNotification *)notification
{
    NetworkStatus status = self.internetReachability.currentReachabilityStatus;
    switch (status) {
        case NotReachable: {
            self.internetAvailable = NO;
        }
            break;
        case ReachableViaWiFi:
        case ReachableViaWWAN: {
            self.internetAvailable = YES;
        }
            break;
            
        default: {
            NSAssert(NO, @"Unknown Status");
        }
            break;
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:kInternetAvailabilityStatusChangedNotification object:nil];
}
@end
